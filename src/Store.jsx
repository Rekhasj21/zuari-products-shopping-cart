import { configureStore } from "@reduxjs/toolkit";
import ShoppingCartReducer from "./components/Redux/ShoppingCartSlice";

const store = configureStore({
  reducer: {
    products: ShoppingCartReducer,
  },
});

export default store;
