import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listOfProducts: [
    {
      id: 11,
      imageUrl: "./pes1.jpg",
      productName: "Adbor",
      price: 140,
    },
    {
      id: 12,
      imageUrl: "./pes2.jpg",
      productName: "Atom-61",
      price: 250,
    },
    {
      id: 13,
      imageUrl: "./pes3.jpg",
      productName: "Avishkar",
      price: 225,
    },
    {
      id: 14,
      imageUrl: "./pes4.jpg",
      productName: "Bhoomitra",
      price: 225,
    },
    {
      id: 15,
      imageUrl: "./pes5.jpg",
      productName: "Bio Gold Plus",
      price: 725,
    },
    {
      id: 1,
      imageUrl: "./product1.jpg",
      productName: "DAP",
      price: 400,
    },
    {
      id: 2,
      imageUrl: "./product2.jpg",
      productName: "UREA 46% N",
      price: 500,
    },
    {
      id: 3,
      imageUrl: "./product3.jpg",
      productName: "COMPOST ",
      price: 400,
    },
    {
      id: 4,
      imageUrl: "./product4.jpg",
      productName: "UREA",
      price: 660,
    },
    {
      id: 5,
      imageUrl: "./product5.jpg",
      productName: "GLYSAAN 41",
      price: 660,
    },
    {
      id: 6,
      imageUrl: "./product6.jpg",
      productName: "RESTORE 2",
      price: 660,
    },
    {
      id: 7,
      imageUrl: "./product7.jpg",
      productName: "NAVARATNA",
      price: 660,
    },
    {
      id: 8,
      imageUrl: "./product8.jpg",
      productName: "BIO FERTILISER",
      price: 660,
    },
    {
      id: 9,
      imageUrl: "./product9.jpg",
      productName: "SDP",
      price: 660,
    },
    {
      id: 10,
      imageUrl: "./product10.jpg",
      productName: "SAMPANNA",
      price: 660,
    },
  ],
  cartItems: [],
};

const ShoppingCartSlice = createSlice({
  name: "shoppingCart",
  initialState,
  reducers: {
    addItem: (state, { payload }) => {
      const existingItem = state.cartItems.find(
        (item) => item.id === payload.id
      );

      if (existingItem) {
        existingItem.quantity += 1;
      } else {
        state.cartItems.push({ ...payload, quantity: 1 });
      }
    },
    addToCart: (state, { payload }) => {
      const existingItem = state.cartItems.find((item) => item.id === payload);

      if (existingItem) {
        existingItem.quantity += 1;
      }
    },
    removeItem: (state, { payload }) => {
      const existingItem = state.cartItems.find((item) => item.id === payload);

      if (existingItem && existingItem.quantity > 1) {
        existingItem.quantity -= 1;
      } else {
        state.cartItems = state.cartItems.filter((item) => item.id !== payload);
      }
    },
   },
});

export const { addItem, removeItem, addToCart } = ShoppingCartSlice.actions;
export default ShoppingCartSlice.reducer;
