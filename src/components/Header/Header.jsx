import React from "react";
import { Link } from "react-router-dom";
import { BsCartFill } from "react-icons/bs";
import "./Header.css";
import { useSelector } from "react-redux";

function Header() {
  const { cartItems, listOfProducts } = useSelector((store) => store.products);

  const cartItemsCount = cartItems.reduce((total, item) => {
    const product = listOfProducts.find((p) => p.id === item.id);
    if (product) {
      return total + item.quantity;
    }
    return total;
  }, 0);

  return (
    <nav className="navbar">
      <Link to="/">
        <img src="/farmhub-logo-1.png" alt="logo" className="logo" />
      </Link>
      <div className="cart-heading-card">
        <div className="cart-icon">
          <span className="quantity">{cartItemsCount}</span>
          <Link to="/cart">
            <BsCartFill fill="white" size={40} />
          </Link>
          <h1 className="fs-4 text-light">Cart</h1>
        </div>

        <div className="heading-card">
          <h1 className="heading">INDIA'S LEADING</h1>
          <h1 className="heading"> AGRITECH COMPANY</h1>
        </div>
      </div>
    </nav>
  );
}

export default Header;
