import React from "react";
import "./Video.css";

function Video({ list }) {
  const { videoUrl, id } = list;
  console.log(list);
  return (
    <div className="modal-container">
      <button
        type="button"
        className="btn "
        data-bs-toggle="modal"
        data-bs-target={`#exampleModal${id}`}
      >
        <img src={`/youtube${id}.jpg`} className="pesticide-image" />
      </button>
      <h1 className="fs-4">Youtube Video: {id}</h1>
      <div
        className="modal fade"
        id={`exampleModal${id}`}
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-xl">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="ratio ratio-21x9">
                <iframe
                  src={`https://www.youtube.com/embed/${videoUrl}`}
                  title="YouTube video"
                  allowFullScreen=""
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Video;
