import React from "react";
import { useSelector } from "react-redux";
import DisplayCartItems from "./DisplayCartItems";

function AddToCartPage() {
  const { cartItems, listOfProducts } = useSelector((store) => store.products);
  console.log(cartItems);
  const totalPrice = cartItems.reduce((total, item) => {
    const product = listOfProducts.find((p) => p.id === item.id);
    if (product) {
      return total + product.price * item.quantity;
    }
    return total;
  }, 0);

  const handleCheckout = () => {
    alert(`Total Price : ${totalPrice}`);
  };
  return (
    <div className="cart-display-container">
      <div className="m-4">
        {cartItems &&
          cartItems.map((item) => (
            <DisplayCartItems key={item.id} list={item} />
          ))}
      </div>
      <div className="checkout-card">
        <h1 className="fs-4 text-center mt-5">
          Total Products in Cart: {cartItems.length}
        </h1>
        <h1 className="fs-4">Sub-total:{totalPrice}</h1>
        <button
          className="btn btn-success align-self-center"
          onClick={handleCheckout}
        >
          Checkout
        </button>
      </div>
    </div>
  );
}

export default AddToCartPage;
