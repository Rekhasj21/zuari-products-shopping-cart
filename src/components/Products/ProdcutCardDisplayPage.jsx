import React from "react";
import { Link } from "react-router-dom";

function ProdcutCardDisplayPage({ product }) {
  const { productName, imageUrl, id,price } = product;

  return (
    <div className="card ml-3 me-3" style={{ width: "30rem", height: "20rem" }}>
      <img
        src={imageUrl}
        className="card-img-top product-image  mb-5"
      />
      <div className="card-body">
        <h5 className="card-title">{productName}</h5>
        <h5 className="card-title">${price}.00</h5>
        <Link to={`/products/${id}`}>
          <button className="btn btn-success">Know More</button>
        </Link>
      </div>
    </div>
  );
}

export default ProdcutCardDisplayPage;
