import React from "react";
import Video from "../VideoDisplay/Video";
import "./KnowMoreDetails.css";

const videoList = [
  { id: 1, imageUrl: "/youtube1.jpg", videoUrl: "wz6KgJ2QVZE" },
  { id: 2, imageUrl: "/youtube2.jpg", videoUrl: "L9YY5AlPP5g" },
  { id: 3, imageUrl: "/youtube3.jpg", videoUrl: "kT0Lrpywf38" },
];

function KnowMoreDetails() {
  return (
    <div className="modal-container">
      <h1>Check videos on how to use the Pesticide</h1>
      <div className="videoCard">
        {videoList.map((video) => {
          return <Video key={video.id} list={video} />;
        })}
      </div>
    </div>
  );
}

export default KnowMoreDetails;
