import React, { useState } from "react";
import "./HomePage.css";
import PlantDetailsPage from "./PlantDetailsPage";

const angles = [180, 240, 300, 360, 90, 130];

function HomePage() {
  const [zoomData, setZoomData] = useState("");
  const [isDisplay, setIsDisplay] = useState(false);
  const [selectedArea, setSelectedArea] = useState(null);

  const imageWidth = 200;
  const imageHeight = 200;
  const centerX = imageWidth / 2;
  const centerY = imageHeight / 2;
  const radius = Math.min(imageWidth, imageHeight) / 4;

  const calculateCircleCoordinates = (centerX, centerY, radius, angle) => {
    const radians = (angle * Math.PI) / 90;
    const x = centerX + radius * Math.cos(radians);
    const y = centerY + radius * Math.sin(radians);
    return `${x},${y},${radius}`;
  };

  const handleAreaClick = (index, angle) => {
    setIsDisplay(true);
    setZoomData(`/leafd${index + 1}`);
    setSelectedArea(index);
  };

  return (
    <div className="home-container">
        <div className="home-card">
          <img src="/mangoPlant.png" alt="MDN infographic" className="image " />
          {angles.map((angle, index) => (
            <div
              key={angle}
              className={`area-container area${index} ${
                selectedArea === index ? "selected" : ""
              }`}
              onClick={() => handleAreaClick(index, angle)}
            >
              <div
                className="area-overlay"
                style={{
                  left: centerX,
                  top: centerY,
                  transform: `rotate(${angle}deg)`,
                }}
              ></div>
            </div>
          ))}
          <h1>Mango Plant</h1>
        </div>
        <div className="display-card">
          {isDisplay && <PlantDetailsPage zoomData={zoomData} />}
        </div>
      </div>

  );
}

export default HomePage;
