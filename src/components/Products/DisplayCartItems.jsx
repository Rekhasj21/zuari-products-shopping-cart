import React from "react";
import { useSelector } from "react-redux";
import "./DisplayCartItems.css";

function DisplayCartItems({ list }) {
  const { productName, imageUrl, quantity, id } = list;
  const { listOfProducts } = useSelector((store) => store.products);

  const cost = listOfProducts.filter((product) => product.id === id);
  return (
    <div className="cart-items-container" style={{ width: "30rem" }}>
      <img src={`${imageUrl}`} className="cart-image card-img-top" />
      <div className="card-body">
        <h1 className="fs-4">{productName}</h1>
        <p className="fs-6 text-success">Price: {cost[0].price}</p>
        <p className="fs-6 text-success">Quantity:{quantity}</p>
      </div>
    </div>
  );
}

export default DisplayCartItems;
