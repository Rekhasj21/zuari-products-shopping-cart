import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

function BuySingleProduct() {
  const { id } = useParams();
  const { cartItems, listOfProducts } = useSelector((store) => store.products);
  const buyProduct = cartItems.filter((product) => product.id === parseInt(id));
  
  let totalPrice = 0;
  buyProduct.forEach((item) => {
    const product = listOfProducts.find((p) => p.id === item.id);
    if (product) {
      totalPrice += product.price * item.quantity;
    }
  });
  const buySingleProduct = () => {
    alert(`Total Price is ${totalPrice}`);
  };

  return (
    <div className="sinlge-buy-card">
    <div className="card text-center" style={{ width: "30rem", height: "30rem" }}>
      <img
        src={`/${buyProduct[0].imageUrl}`}
        className="card-img-top product-image"
      />
      <div>
        <h1 className="fs-4">{cartItems[0].productName}</h1>
        <p>Quantity : {cartItems[0].quantity}</p>
        <button className="btn btn-success" onClick={buySingleProduct}>
          Checkout
        </button>
      </div>
    </div>
    </div>
  );
}

export default BuySingleProduct;
