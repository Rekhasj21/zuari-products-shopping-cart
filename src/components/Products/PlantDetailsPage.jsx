import React from "react";
import { Link } from "react-router-dom";
import "./PlantDetailsPage.css";

function PlantDetailsPage({ zoomData }) {

  return (
    <div className="plant-container">
      <h1 className="fs-3">Plant Disease Details</h1>
      <img
        src={`${zoomData}.jpg`}
        alt="leaf"
        className="plant-d image-animation mb-5"
      />
      <div className="plant-details-card">
        <h1 className="fs-3 text-success">Description</h1>
        <p className="fs-6 text-success">
          Mango tree diseases can be caused <br />
          by various pathogens such as fungi, <br />
          bacteria, and viruses,affecting the <br />
          health and productivity of the trees.
          <br />
          One common disease of mango trees is <br />
          anthracnose,caused by the fungus <br />
          Colletotrichum gloeosporioides.
        </p>
        <div>
          <Link to="/products">
            <button className="btn btn-success m-1">Buy Pesticides</button>
          </Link>
          <Link to="/details">
            <button className="btn btn-success m-1">Know More</button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default PlantDetailsPage;
