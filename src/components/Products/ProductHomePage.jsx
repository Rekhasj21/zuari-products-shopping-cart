import React from "react";
import { useSelector } from "react-redux";
import ProdcutCardDisplayPage from "./ProdcutCardDisplayPage";
import "./ProductHomePage.css";

function ProductHomePage() {
  const { listOfProducts } = useSelector((store) => store.products);

  return (
    <div className="products-container">
      <div className="banner-card">
        <div>
          <h1 className="text-light fs-5">Jai Kisaan SPN</h1>
          <p className="text-light fs-6 ">
            A wide range of Speciality Plant
            <br /> Nutrients for every crop & soil condition
          </p>
        </div>
      </div>
      <h1 className="m-2">List of Products</h1>
      <div className="product-container">
        {listOfProducts.map((product) => (
          <ProdcutCardDisplayPage product={product} key={product.id} />
        ))}
      </div>
    </div>
  );
}

export default ProductHomePage;
