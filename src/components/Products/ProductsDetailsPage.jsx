import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { addItem, removeItem, addToCart } from "../Redux/ShoppingCartSlice";
import "./ProductsDetailsPage.css";

function ProductsDetailsPage() {
  const { listOfProducts, cartItems } = useSelector((store) => store.products);
  const dispatch = useDispatch();
  const { id } = useParams();
  const productId = parseInt(id);
  const cartItem = cartItems.find((item) => item.id === productId);
  const quantity = cartItem ? cartItem.quantity : 1;

  const filteredProduct = listOfProducts.find(
    (product) => product.id === productId
  );

  const onIncrement = () => {
    dispatch(
      addItem({
        id: filteredProduct.id,
        productName: filteredProduct.productName,
        imageUrl: filteredProduct.imageUrl,
        quantity: quantity,
      })
    );
  };

  const onDecrement = () => {
    dispatch(removeItem(filteredProduct.id));
  };

  const handleAddToCart = () => {
    console.log("CartItems", cartItems);
    dispatch(
      addToCart({
        id: filteredProduct.id,
        productName: filteredProduct.productName,
        imageUrl: filteredProduct.imageUrl,
        quantity: quantity,
      })
    );
  };

  return (
    <div className="cart-container">
      <div className="product-details-container">
        <div
          className="product-details-card"
          style={{ width: "30rem", height: "30rem" }}
        >
          <img
            src={`/${filteredProduct?.imageUrl}`}
            className="card-img-top product-image"
          />
          <h3 className="fs-4 text-success mt-3"> Product Description</h3>
          <ul className="fs-6 text-success">
            <li>Highest phosphorus content (61 %) amongst the fertilizers.</li>
            <li>
              Better control over nitrogen availability as nitrogen is available
              in Ammoniacal form.
            </li>
            <li>
              Ammonium ion reduces pH of soil surrounding root system, which in
              turn helps quick and easy absorption of phosphorus from the soil.
            </li>
            <li>
              Can be mixed with all water soluble fertilizers except calcium
              containing group of fertilizers.
            </li>
            <li>
              Helps in root development, flower setting and panicle emergence in
              plants.
            </li>
            <li>
              Dosage: <br />
              Fertigation: 1 to 3 kg per acre <br />
              Foliar application: 6 gm per liter of water.
            </li>
            <li> 2 to 3 sprays at an interval of 10 to 15 days. </li>
            <li>Available in 1, 10 and 25 kg bags.</li>
          </ul>
        </div>
        <div className="add-cart-card">
          <h1 className="fs-4">{filteredProduct?.productName}</h1>
          <p className="fs-6 text-success">
            Price of the Product:{filteredProduct?.price}
          </p>
          <div className="button-card">
            <button
              className="btn btn-outline-success m-2"
              onClick={onDecrement}
            >
              -
            </button>
            <p className="m-2">{quantity}</p>
            <button
              className="btn btn-outline-success m-2"
              onClick={onIncrement}
            >
              +
            </button>
          </div>
          <div>
            <button className="btn btn-success m-2" onClick={handleAddToCart}>
              Add to Cart
            </button>
            <Link to={`/buy/${id}`}>
              <button className="btn btn-success m-2">Quik Buy</button>
            </Link>
          </div>
          <div>
            <h1 className="fs-6 m-2">Check Availability</h1>
            <input type="number" />
            <button className="btn btn-secondary">Check</button>
            <p className="pincode-check">
              Enter your Pincode to check Delivery Options available in your
              Area
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductsDetailsPage;
