import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import AddToCartPage from "./components/Products/AddToCartPage";
import BuySingleProduct from "./components/Products/BuySingleProduct";
import HomePage from "./components/Products/HomePage";
import KnowMoreDetails from "./components/Products/KnowMoreDetails";
import ProductHomePage from "./components/Products/ProductHomePage";
import ProductsDetailsPage from "./components/Products/ProductsDetailsPage";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/products" element={<ProductHomePage />} />
        <Route path="/details" element={<KnowMoreDetails />} />
        <Route path="/products/:id" element={<ProductsDetailsPage />} />
        <Route path="/cart" element={<AddToCartPage />} />
        <Route path="/buy/:id" element={<BuySingleProduct />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
